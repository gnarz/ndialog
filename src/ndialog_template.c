/* ndialog_template.c
 *
 * simple interface to native dialogs, implementation template
 *
 * Gunnar Zötl <gz@tset.de>, 2015
 * Released under the terms of the MIT license. See file LICENSE for details.
 */

#include "ndialog.h"
#include "ndialog_util.h"

ndialog_btntype ndialog_messagebox(const char* const title, const char* const message, const ndialog_msgtype msgtype, const ndialog_dlgtype dlgtype)
{
	_ndutil_clear_error();
	_ndutil_set_error(NDIALOG_ERR_NOTIMP);
	return 0;
}

const char *ndialog_savefile(const char* const title, const char* const path, const char **filters, const int allowcreatefolder, const int askoverwrite)
{
	_ndutil_clear_error();
	_ndutil_set_error(NDIALOG_ERR_NOTIMP);
	return NULL;
}

const char *ndialog_openfile(const char* const title, const char* const path, const char **filters, const int allowmultiple, int *nres)
{
	_ndutil_clear_error();
	_ndutil_set_error(NDIALOG_ERR_NOTIMP);
	return NULL;
}

const char * ndialog_selectfolder(const char* const title, const char* const path, const int allowcreatefolder)
{
	_ndutil_clear_error();
	_ndutil_set_error(NDIALOG_ERR_NOTIMP);
	return NULL;
}
