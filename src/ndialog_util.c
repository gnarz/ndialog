/* ndialog_util.c
 *
 * simple interface to native dialogs, utilities
 *
 * Gunnar Zötl <gz@tset.de>, 2015
 * Released under the terms of the MIT license. See file LICENSE for details.
 */

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#ifdef _WIN32

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#undef WIN32_LEAN_AND_MEAN

/* returns the utf-8 encoded string received as argument to utf-16.
 * Returns NULL if the conversion was not successfull.
 */
static const LPWSTR utf8_to_utf16(const LPCSTR u8str)
{
	int u8len = strlen(u8str) + 1;
	int u16len = MultiByteToWideChar(CP_UTF8, 0, u8str, u8len, 0, 0);
	LPWSTR u16str = NULL;
	if (u16len > 0) {
		u16str = calloc(u16len, sizeof(wchar_t));
		int ok = MultiByteToWideChar(CP_UTF8, 0, u8str, u8len, u16str, u16len);
		if (ok == 0 || ok != u16len) {
			free(u16str);
			u16str = NULL;
		}
	}
	return u16str;
}

#else

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#endif

#include "ndialog.h"
#include "ndialog_util.h"

int _ndutil_is_file(const char* const path)
{
#ifdef _WIN32
	LPWSTR wpath = utf8_to_utf16(path);
	DWORD attr = 0;
	if (wpath) {
		attr = GetFileAttributesW(wpath);
		free(wpath);
	} else {
		attr = GetFileAttributes(path);
	}

	if (attr != 0xFFFFFFFF) {
		DWORD DontWantAttrs = FILE_ATTRIBUTE_DIRECTORY | FILE_ATTRIBUTE_DEVICE | FILE_ATTRIBUTE_REPARSE_POINT | 0xFFFFC000;
		return (attr & DontWantAttrs) == 0;
	}
	return 0;
#else
	struct stat attr;
	if (stat(path, &attr) == 0) {
		return S_ISREG(attr.st_mode);
	}
	return 0;
#endif
}

int _ndutil_is_dir(const char* const path)
{
#ifdef _WIN32
	LPWSTR wpath = utf8_to_utf16(path);
	DWORD attr = 0;
	if (wpath) {
		attr = GetFileAttributesW(wpath);
		free(wpath);
	} else {
		attr = GetFileAttributes( path );
	}

	if ( attr != 0xFFFFFFFF ) {
		return (attr & FILE_ATTRIBUTE_DIRECTORY) != 0;
	}
	return 0;
#else
	struct stat attr;
	if (stat(path, &attr) == 0) {
		return S_ISDIR(attr.st_mode);
	}
	return 0;
#endif
}

static int ndialog_isslash(const char c)
{
#ifdef _WIN32
	return c == '\\' || c == '/';
#else
	return c == '/';
#endif
}

static const char* ndialog_lastslash(const char* path, int before)
{
	if (!path || !*path) { return NULL; }
	int sp = strlen(path) - 1;
	if (before <= 0 || before > sp) { before = sp; }
	const char* ptr = path + before;
	while (ptr >= path && !ndialog_isslash(*ptr)) {
		--ptr;
	}
	if (ptr < path || !ndialog_isslash(*ptr)) {
		return NULL;
	}
	return ptr;
}

static const char* ndialog_lastdot(const char* path, int before)
{
	if (!path || !*path) { return NULL; }
	int sp = strlen(path) - 1;
	if (before <= 0 || before > sp) { before = sp; }
	const char* ptr = path + before;
	while (ptr >= path && *ptr != '.') {
		--ptr;
	}
	if (ptr < path || *ptr != '.') {
		return NULL;
	}
	return ptr;
}

int _ndutil_dirname(const char* const path, char *buf, int buflen)
{
	const char* slash = ndialog_lastslash(path, 0);
	if (!slash) return 0;
	int len = slash - path;
	if (buf) {
		if (len + 1 > buflen) { return -1; }
		strncpy(buf, path, len);
		buf[len] = 0;
	}
	return 1;
}

int _ndutil_filename(const char* const path, char *buf, int buflen)
{
	const char* start = ndialog_lastslash(path, 0);
	if (start[1] == 0) { return 0; }
	if (!start) {
		start = path;
	} else {
		start += 1;
	}
	int len = strlen(start);
	if (buf) {
		if (len + 1 > buflen) { return -1; }
		strncpy(buf, start, len);
		buf[len] = 0;
	}
	return 1;
}

int _ndutil_basename(const char* const path, const char* const extn, char *buf, int buflen)
{
	int plen = strlen(path) + 1;
	char *lbuf = malloc(plen);
	int ok = 0;
	if (_ndutil_filename(path, lbuf, plen) > 0) {
		ok = 1;
		int flen = strlen(lbuf);
		int elen = extn ? strlen(extn) : 0;
		if (extn && elen < flen) {
			int cmp = strcmp(lbuf + flen - elen, extn);
			if (cmp == 0) { flen -= elen; }
		}
		if (buf) {
			if (flen + 1 > buflen) {
				free(lbuf);
				return -1;
			}
			strncpy(buf, lbuf, flen);
			buf[flen] = 0;
		}
	}
	free(lbuf);
	return ok;
}

int _ndutil_fileextn(const char* const path, char *buf, int buflen)
{
	const char* start = ndialog_lastdot(path, 0);
	if (!start) { return 0; }
	int len = strlen(start);
	if (buf) {
		if (len + 1 > buflen) { return -1; }
		strncpy(buf, start, len);
		buf[len] = 0;
	}
	return 1;
}

int _ndutil_separate_extns(const char* extns, char *buf, int buflen)
{
	if (!extns || !*extns) return 0;
	int xlen = strlen(extns);
	if (xlen + 1 > buflen) return -1;
	char *to = buf;
	int nextns = 0, i;
	for (i = 0; i < xlen; ++i) {
		int c = extns[i];
		if (isspace(c)) {
			continue;
		} else if (c == ',') {
			if (to > buf && to[-1]) {
				*to++ = 0;
				nextns += 1;
			}
		} else if (c != ',') {
			*to++ = c;
		}
	}
	if (to > buf && to[-1] != 0) {
		*to = 0;
		nextns += 1;
	}

	return nextns;
}

static ndialog_errtype _ndutil_last_error = NDIALOG_ERR_NONE;

void _ndutil_set_error(ndialog_errtype err)
{
	if (err > NDIALOG_ERR_UNKNOWN) {
		err = NDIALOG_ERR_UNKNOWN;
	}
	_ndutil_last_error = err;
}

void _ndutil_clear_error()
{
	_ndutil_last_error = NDIALOG_ERR_NONE;
}

ndialog_errtype ndialog_lasterror()
{
	ndialog_errtype err = _ndutil_last_error;
	_ndutil_clear_error();
	return err;
}
