/* ndialog_windows.c
 *
 * simple interface to native dialogs, windows implementation
 *
 * Gunnar Zötl <gz@tset.de>, 2015
 * Released under the terms of the MIT license. See file LICENSE for details.
 */

#include <windows.h>
#include <Shlobj.h>
#include <Shobjidl.h>
#include <stdio.h>
#include <stdlib.h>
#include "ndialog.h"
#include "ndialog_util.h"

static unsigned long msgtypemap[] = {
	MB_ICONINFORMATION,
	MB_ICONWARNING,
	MB_ICONERROR,
	MB_ICONQUESTION
};

static unsigned long dlgtypemap[] = {
	MB_OK,
	MB_OKCANCEL,
	MB_YESNO
};

int ndialog_init()
{
	return 1;
}

void ndialog_exit()
{
	// empty
}

static const LPWSTR ndialog_utf8_to_wchar(const char* in)
{
	if (!in) { return NULL; }

	int wlen = MultiByteToWideChar(CP_UTF8, 0, in, -1, NULL, 0);
	if (wlen <= 0) {
		if (*in) { _ndutil_set_error(NDIALOG_ERR_INVENC); }
		return NULL;
	}
	LPWSTR out = calloc(wlen, sizeof(wchar_t));
	if (!out) {
		_ndutil_set_error(NDIALOG_ERR_NOMEM);
		return NULL;
	}
	(void) MultiByteToWideChar(CP_UTF8, 0, in, -1, out, wlen);
	return out;
}

static int ndialog_utf8_as_wchar_len(const char* in)
{
	if (!in) { return 0; }
	return MultiByteToWideChar(CP_UTF8, 0, in, -1, NULL, 0) - 1;
}

static const int ndialog_utf8_as_wchar_cpy(LPWSTR dest, int size, const char* src)
{
	return MultiByteToWideChar(CP_UTF8, 0, src, -1, dest, size);
}

static const char *ndialog_wchar_to_utf8(const LPWSTR in)
{
	if (!in) { return NULL; }

	int len = WideCharToMultiByte(CP_UTF8, 0, in, -1, NULL, 0, NULL, NULL);
	if (len <= 0) {
		if (*in) { _ndutil_set_error(NDIALOG_ERR_INVENC); }
		return NULL;
	}
	char *out = calloc(len, sizeof(char));
	if (!out) {
		_ndutil_set_error(NDIALOG_ERR_NOMEM);
		return NULL;
	}
	(void) WideCharToMultiByte(CP_UTF8, 0, in, -1, out, len, NULL, NULL);
	return out;
}

static int ndialog_wchar_as_utf8_len(const LPWSTR in)
{
	if (!in) { return 0; }
	return WideCharToMultiByte(CP_UTF8, 0, in, -1, NULL, 0, NULL, NULL) - 1;
}

static const int ndialog_wchar_as_utf8_cpy(char *dest, int size, const LPWSTR src)
{
	return WideCharToMultiByte(CP_UTF8, 0, src, -1, dest, size, NULL, NULL);
}

static int ndialog_wstrlen(LPWSTR str)
{
	if (!str) { return 0; }
	int l = 0;
	while (str[l]) {
		++l;
	}
	return l;
}

ndialog_btntype ndialog_messagebox(const char* const title, const char* const message, const ndialog_msgtype msgtype, const ndialog_dlgtype dlgtype)
{
	_ndutil_clear_error();
	wchar_t *wtitle = ndialog_utf8_to_wchar(title);
	if (title && *title && !wtitle) { return NDIALOG_ERR_NOMEM; }
	const wchar_t *wmsg = ndialog_utf8_to_wchar(message);
	if (message && *message && !wmsg) {
		free(wtitle);
		return NDIALOG_BTN_NONE;
	}

	int ok = MessageBoxW(NULL, wmsg, wtitle, msgtypemap[msgtype] | dlgtypemap[dlgtype]);

	switch (ok) {
		case IDOK: return NDIALOG_BTN_OK;
		case IDCANCEL: return NDIALOG_BTN_CANCEL;
		case IDYES: return NDIALOG_BTN_YES;
		case IDNO: return NDIALOG_BTN_NO;
		default:;
	}

	if (wtitle) { free((void*)wtitle); }
	if (wmsg) { free((void*)wmsg); }

	return NDIALOG_BTN_NONE;
}

static LPWSTR ndialog_mkfilterstring(const char** filters)
{
	if (!filters || !*filters) { return NULL; }
	int fslen = 2, mxlen = 0;
	int i = 0;
	while (filters[i]) {
		/* name */
		fslen += ndialog_utf8_as_wchar_len(filters[i]) + 1;
		++i;
		if (!filters[i]) return NULL; /* invalid filter list */
		/* extensions */
		fslen += ndialog_utf8_as_wchar_len(filters[i]) + 1;
		char *extp = (char*)filters[i];
		int extlen = strlen(extp);
		mxlen = mxlen > extlen ? mxlen : extlen;
		fslen += 2; /* "*." */
		while (*extp) {
			if (*extp++ == ',') fslen += 2;
		}
		++i;
	}
	LPWSTR fstring = calloc(fslen, sizeof(wchar_t));
	if (!fstring) {
		_ndutil_set_error(NDIALOG_ERR_NOMEM);
		return NULL;
	}
	wchar_t *p = fstring;
	int size = fslen;
	i = 0;
	char *extns = malloc(mxlen + 1);
	if (!extns) {
		free(fstring);
		_ndutil_set_error(NDIALOG_ERR_NOMEM);
		return NULL;
	}
	char *fxbuf = malloc(fslen);
	if (!fxbuf) {
		free(fstring);
		free(extns);
		_ndutil_set_error(NDIALOG_ERR_NOMEM);
		return NULL;
	}
	while (filters[i]) {
		/* name */
		int len = ndialog_utf8_as_wchar_cpy(p, size, filters[i]);
		p += len;
		size -= len;
		++i;
		/* extensions */
		int nextns = _ndutil_separate_extns(filters[i], extns, mxlen + 1);
		char *extp = extns;
		char *xtop = fxbuf;
		int extno;
		for (extno = 0; extno < nextns; ++extno) {
			sprintf(xtop, "*.%s;", extp);
			xtop += strlen(xtop);
			extp += strlen(extp) + 1;
		}
		if (nextns > 0) xtop[-1] = 0;
		len = ndialog_utf8_as_wchar_cpy(p, size, fxbuf);
		p += len;
		size -= len;
		++i;
	}
	*p = 0;

	free(fxbuf);
	free(extns);
	return fstring;
}

static void ndialog_init_openfilenamestruct(OPENFILENAMEW *ofn, LPWSTR wtitle, LPWSTR wpath, LPWSTR wfilters, LPWSTR buf, int bufsize)
{
	memset(ofn, 0, sizeof(OPENFILENAMEW));
	ofn->lStructSize = sizeof(OPENFILENAMEW);
	ofn->lpstrFilter = wfilters;
	ofn->nFilterIndex = 1;
	ofn->lpstrFile = buf;
	ofn->nMaxFile = bufsize;
	ofn->lpstrTitle = wtitle;
	ofn->lpstrInitialDir = wpath;
	ofn->Flags = OFN_EXPLORER | OFN_ENABLESIZING;
}

const char *ndialog_savefile(const char* const title, const char* const path, const char **filters, const int allowcreatefolder, const int askoverwrite)
{
	_ndutil_clear_error();
	static const char *res = NULL;
	wchar_t buf[4096];
	buf[0] = 0;
	LPWSTR wtitle = ndialog_utf8_to_wchar(title);
	if (title && *title && !wtitle) { return NULL; }
	LPWSTR wpath = NULL;
	if (path && _ndutil_is_file(path)) {
		char fnbuf[MAX_PATH];
		_ndutil_dirname(path, fnbuf, MAX_PATH);
		wpath = ndialog_utf8_to_wchar(fnbuf);
		_ndutil_filename(path, fnbuf, MAX_PATH);
		ndialog_utf8_as_wchar_cpy(buf, 4096, fnbuf);
	} else {
		wpath = ndialog_utf8_to_wchar(path);
	}
	if (path && *path && !wpath) {
		free(wtitle);
		return NULL;
	}
	LPWSTR wfilters = ndialog_mkfilterstring(filters);
	if (filters && *filters && !wfilters) {
		free(wtitle);
		free(wpath);
		return NULL;
	}

	OPENFILENAMEW ofn;
	ndialog_init_openfilenamestruct(&ofn, wtitle, wpath, wfilters, buf, 4096);
	ofn.Flags |= OFN_ENABLESIZING | OFN_NOTESTFILECREATE;
	if (askoverwrite) {
		ofn.Flags |= OFN_OVERWRITEPROMPT;
	}
	/* seems to be always possible on windows with GetSaveFileName()
	if (allowcreatefolder) {
		???
	}
	*/

	int ok = GetSaveFileNameW(&ofn);

	if (wfilters) free(wfilters);
	if (wpath) free(wpath);
	if (wtitle) free(wtitle);

	if (!ok) { return NULL; }

	if (res) { free((char*)res); }
	res = ndialog_wchar_to_utf8(buf);

	return res;
}

const char *ndialog_openfile(const char* const title, const char* const path, const char **filters, const int allowmultiple, int *nres)
{
	_ndutil_clear_error();
	if (nres) {
		*nres = 0;
	} else if (allowmultiple) {
		return NULL;
	}

	static const char *res = NULL;

	wchar_t buf[4096]; // is this enough?
	buf[0] = 0;
	LPWSTR wtitle = ndialog_utf8_to_wchar(title);
	LPWSTR wpath = NULL;
	if (path && _ndutil_is_file(path)) {
		char fnbuf[MAX_PATH];
		_ndutil_dirname(path, fnbuf, MAX_PATH);
		wpath = ndialog_utf8_to_wchar(fnbuf);
		_ndutil_filename(path, fnbuf, MAX_PATH);
		ndialog_utf8_as_wchar_cpy(buf, 4096, fnbuf);
	} else {
		wpath = ndialog_utf8_to_wchar(path);
	}
	LPWSTR wfilters = ndialog_mkfilterstring(filters);

	OPENFILENAMEW ofn;
	ndialog_init_openfilenamestruct(&ofn, wtitle, wpath, wfilters, buf, 4096);
	ofn.Flags |= OFN_ENABLESIZING;
	if (allowmultiple) {
		ofn.Flags |= OFN_ALLOWMULTISELECT;
	}

	int ok = GetOpenFileNameW(&ofn);

	if (wfilters) free(wfilters);
	if (wpath) free(wpath);
	if (wtitle) free(wtitle);

	if (!ok) { return NULL; }

	if (res) { free((char*)res); }
	if (!allowmultiple) {
		if (nres) { *nres = 1; }
		res = ndialog_wchar_to_utf8(buf);
	} else {
		int nch = 0;
		int wplen = ndialog_wstrlen(buf);
		int u8plen = ndialog_wchar_as_utf8_len(buf);
		wchar_t *p = buf + wplen + 1;
		while (*p) {
			int l = ndialog_wstrlen(p);
			nch += u8plen + 1 + ndialog_wchar_as_utf8_len(p) + 1;
			p += l + 1;
		}
		nch += 1;
		res = malloc(nch);
		char *resp = (char*) res;
		p = buf + wplen + 1;
		if (*p) {
			while (*p) {
				int l = ndialog_wstrlen(p);
				ndialog_wchar_as_utf8_cpy(resp, nch, buf);
				resp += u8plen;
				*resp++ = '\\';
				nch -= u8plen + 1;
				ndialog_wchar_as_utf8_cpy(resp, nch, p);
				int dl = strlen(resp);
				resp += dl + 1;
				nch -= dl + 1;
				p += l + 1;
				*nres += 1;
			}
		} else {
			*nres = 1;
			res = ndialog_wchar_to_utf8(buf);
		}
	}

	return res;
}

/* holy maccaroni... the stuff that is necessary here... */
const char * ndialog_selectfolder(const char* const title, const char* const path, const int allowcreatefolder)
{
	_ndutil_clear_error();
	wchar_t buf[MAX_PATH + 1];
	static const char* res = NULL;

	IMalloc * imalloc = 0;
	if (FAILED(SHGetMalloc(&imalloc))) {
		return NULL;
	}

	LPWSTR wpath = NULL;
	if (_ndutil_is_file(path)) {
		int blen = strlen(path) + 1;
		char *buf = malloc(blen);
		_ndutil_dirname(path, buf, blen);
		wpath = ndialog_utf8_to_wchar(buf);
		free(buf);
	} else {
		wpath = ndialog_utf8_to_wchar(path);
	}

	LPITEMIDLIST root = NULL;
	if (wpath) {
		LPSHELLFOLDER ishellfolder = NULL;
		if (SHGetDesktopFolder(&ishellfolder) == NOERROR) {
			ishellfolder->lpVtbl->ParseDisplayName (ishellfolder, NULL, NULL, wpath, NULL, &root, NULL);
		}
		ishellfolder->lpVtbl->Release(ishellfolder);
	}

	LPWSTR wtitle = ndialog_utf8_to_wchar(title);

	BROWSEINFOW lpbi;
	memset(&lpbi, 0, sizeof(lpbi));
	lpbi.pidlRoot = root;
	lpbi.pszDisplayName = buf;
	lpbi.lpszTitle = wtitle;
	lpbi.ulFlags = BIF_USENEWUI | (allowcreatefolder ? 0 : BIF_NONEWFOLDERBUTTON);

	PIDLIST_ABSOLUTE pidl = SHBrowseForFolderW(&lpbi);
	if (!pidl) {
	    if (root) { imalloc->lpVtbl->Free(imalloc, pidl); }
	    imalloc->lpVtbl->Release(imalloc);
		if (wpath) free(wpath);
		if (wtitle) free(wtitle);
	    return NULL;
	}
	SHGetPathFromIDListW(pidl, buf);

    imalloc->lpVtbl->Free(imalloc, pidl);
    if (root) { imalloc->lpVtbl->Free(imalloc, pidl); }
    imalloc->lpVtbl->Release(imalloc);
	if (wpath) free(wpath);
	if (wtitle) free(wtitle);

	if (res) { free((char*)res); }
	res = ndialog_wchar_to_utf8(buf);

	return res;
}
