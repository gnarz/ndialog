/* test_ndialog.c
 *
 * tests for ndialog, the simple interface to native dialogs
 *
 * Gunnar Zötl <gz@tset.de>, 2015
 * Released under the terms of the MIT license. See file LICENSE for details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ndialog.h"
#include "ndialog_util.h"

#define NDIALOG_STATIC_FILTERS(...) ((const char*[]){__VA_ARGS__, NULL})

void test_ndialog_utils(const char *fname)
{
	if (!fname) return;

	int blen = strlen(fname) + 1;
	char *buf = malloc(blen);
	if (_ndutil_dirname(fname, buf, blen)) {
		printf("Dirname: %s\n", buf);
	}
	if (_ndutil_filename(fname, buf, blen)) {
		printf("Filename: %s\n", buf);
	}
	if (_ndutil_fileextn(fname, buf, blen)) {
		printf("Extension: %s\n", buf);
	}
	char* extn = strdup(buf);
	if (_ndutil_basename(fname, extn, buf, blen)) {
		printf("Basename (%s): %s\n", extn, buf);
	}
	if (_ndutil_basename(fname, NULL, buf, blen)) {
		printf("Basename (NULL): %s\n", buf);
	}
	if (_ndutil_basename(fname, ".bla", buf, blen)) {
		printf("Basename (.bla): %s\n", buf);
	}
}

int main(int argc, char **argv)
{
	int quit = 0;

	if (!ndialog_init()) {
		printf("ndialog initialisation failed.\n");
		exit(1);
	}

	setvbuf(stdin, 0, _IONBF, 0);

	while (!quit) {
		printf("\f");
		printf("Choose:\n");
		printf("1\tok MessageBox, NULL message\n");
		printf("2\tok-cancel MessageBox, NULL title\n");
		printf("3\tyes-no MessageBox\n");
		printf("4\topen file dialog, NULL title\n");
		printf("5\topen file dialog, multiple selections possible\n");
		printf("6\tsave file dialog, NULL title, no overwrite warning\n");
		printf("7\tsave file dialog, allow create folder, overwrite warning\n");
		printf("8\tselect folder dialog, NULL title\n");
		printf("9\tselect folder dialog, allow create folder\n");
		printf("r\tredraw\n");
		printf("q\tquit\n");

		int c, leave = 0;
		char* openf = NULL;
		while (!leave && (c = fgetc(stdin))) {
			switch (c) {
				case '1': {
					int ok = ndialog_messagebox("OK Messagebox", NULL, NDIALOG_MSG_INFO, NDIALOG_DLG_OK);
					printf("> ok messagebox returned with code %d\n", ok);
					break;
				}
				case '2': {
					int ok = ndialog_messagebox(NULL, "This is the OK-Cancel messagebox", NDIALOG_MSG_ERROR, NDIALOG_DLG_OKCANCEL);
					printf("> ok-cancel messagebox returned with code %d\n", ok);
					break;
				}
				case '3': {
					int ok = ndialog_messagebox("Yes-No Messagebox", "This is the Yes-No messagebox", NDIALOG_MSG_QUESTION, NDIALOG_DLG_YESNO);
					printf("> yes-no messagebox returned with code %d\n", ok);
					break;
				}
				case '4': {
					const char *file = ndialog_openfile(NULL, openf, NDIALOG_STATIC_FILTERS("Text files", "txt", "C-Files", "c,h", "All files", "*"), 0, NULL);
					printf("> open file dialog returned '%s'\nThis will be the default value for all following file dialogs.\n", file ? file : "(null)");
					if (openf) { free(openf); }
					openf = file ? strdup(file) : NULL;
					test_ndialog_utils(openf);
					break;
				}
				case '5': {
					int nres, i;
					const char *file = ndialog_openfile("Open File", openf, NDIALOG_STATIC_FILTERS("Text files", "txt", "C-Files", "c,h", "All files", "*"), 1, &nres);
					printf("> open file dialog (multi) returned %d results:\n", nres);
					char *p = (char*) file;
					for (i = 0; i < nres; ++i) {
						printf("  '%s'\n", p);
						p += strlen(p) + 1;
					}
					break;
				}
				case '6': {
					const char *file = ndialog_savefile(NULL, openf, NDIALOG_STATIC_FILTERS("Text files", "txt", "C-Files", "c,h", "All files", "*"), 0, 0);
					printf("> save file dialog returned '%s'\n", file ? file : "(null)");
					break;
				}
				case '7': {
					const char *file = ndialog_savefile("Save File", openf, NDIALOG_STATIC_FILTERS("Text files", "txt", "C-Files", "c,h", "All files", "*"), 1, 1);
					printf("> save file dialog returned '%s'\n", file ? file : "(null)");
					break;
				}
				case '8': {
					const char *file = ndialog_selectfolder(NULL, openf, 0);
					printf("> open folder dialog returned '%s'\n", file ? file : "(null)");
					break;
				}
				case '9': {
					const char *file = ndialog_selectfolder("Select Folder", openf, 1);
					printf("> open folder dialog returned '%s'\n", file ? file : "(null)");
					break;
				}
				case 'r': leave = 1; break;
				case 'q': leave = 1; quit = 1; break;
				default:;
			}
		}
	}
	
	ndialog_exit();

	exit(0);
}