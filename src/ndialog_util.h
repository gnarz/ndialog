/* ndialog_util.h
 *
 * simple interface to native dialogs, utilities
 *
 * Gunnar Zötl <gz@tset.de>, 2015
 * Released under the terms of the MIT license. See file LICENSE for details.
 */

#ifndef ndialog_util_h
#define ndialog_util_h

/* Function: _ndutil_is_file
 *
 * check whether a path points to a file
 * Arguments:
 *	path	- the path to check
 *
 * Returns:
 *	1 of the path points to a file, 0 if not
 */
int _ndutil_is_file(const char* const path);

/* Function: _ndutil_is_dir
 *
 * check whether a path points to a directory
 * Arguments:
 *	path	- the path to check
 *
 * Returns:
 *	1 of the path points to a directory, 0 if not
 */
int _ndutil_is_dir(const char* const path);

/* Function: _ndutil_dirname
 *
 * extracts the directory name from a path
 *
 * Arguments:
 *	path	- path to extract directory name from
 *	buf		- buffer to place directory name into
 *	buflen	- length of buf
 *
 * Returns:
 *	1 if the directory name could successfully be extracted, and the
 *	directory name in buf, or 0 if not.
 */
int _ndutil_dirname(const char* const path, char *buf, int buflen);

/* Function: _ndutil_filename
 *
 * extracts the filename from a path
 *
 * Arguments:
 *	path	- path to extract filename from
 *	buf		- buffer to place filename into
 *	buflen	- length of buf
 *
 * Returns:
 *	1 if the filename could successfully be extracted, and the filename
 *	in buf, or 0 if not.
 */
int _ndutil_filename(const char* const path, char *buf, int buflen);

/* Function: _ndutil_basename
 *
 * extracts the basename (filename minus extension) name from a path
 *
 * Arguments:
 *	path	- path to extract basename from
 *	extn	- extension to strip from filename
 *	buf		- buffer to place basename into
 *	buflen	- length of buf
 *
 * Returns:
 *	1 if the basename could successfully be extracted, and the basename
 *	in buf, or 0 if not. If the file ends in extn, the extension will be
 *	removed, if it doesn't, or extn is NULL, the complete filename will
 *	be returned.
 */
int _ndutil_basename(const char* const path, const char* const extn, char *buf, int buflen);

/* Function: _ndutil_fileextn
 *
 * extracts the filename extension from a path
 *
 * Arguments:
 *	path	- path to extract filename extension from
 *	buf		- buffer to place filename extension into
 *	buflen	- length of buf
 *
 * Returns:
 *	1 if the filename extension could successfully be extracted, and the
 *	extension in buf, or 0 if not.
 */
int _ndutil_fileextn(const char* const path, char *buf, int buflen);

/* Function: _ndutil_separate_extns
 *
 * separates a comma separated list of extensions and returns the number
 * of extensions found. This is very forgiving in that whitespace is
 * ignored, and empty entries in the list are skipped.
 *
 * Arguments:
 *	extns	- comma separated list of extensions
 *	buf		- buffer to place result into
 *	buflen	- length of buf
 *
 * Returns:
 *	the number of extensions found in extns, plus a \0 separated list of
 *	extensions in buf.
 */
int _ndutil_separate_extns(const char* extns, char *buf, int buflen);

/* Function: _ndutil_set_error
 *
 * set the internal error code to be returned by ndialog_lasterror()
 *
 * Arguments:
 *	err		- error code
 *
 * Returns:
 *	-
 */
void _ndutil_set_error(ndialog_errtype err);

/* Function: _ndutil_clear_error
 *
 * set the internal error code to be returned by ndialog_lasterror()
 * to NDIALOG_ERR_NONE
 *
 * Arguments:
 *	-
 *
 * Returns:
 *	-
 */
void _ndutil_clear_error();

#endif /* ndialog_util_h */
