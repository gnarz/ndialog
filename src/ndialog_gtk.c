/* ndialog_gtk.c
 *
 * simple interface to native dialogs, gtk2/3 implementation
 *
 * Gunnar Zötl <gz@tset.de>, 2015
 * Released under the terms of the MIT license. See file LICENSE for details.
 */

#include "ndialog.h"
#include "ndialog_util.h"
#include <gtk/gtk.h>
#include <string.h>
#include <stdlib.h>

static GtkMessageType msgtypemap[] = {
	GTK_MESSAGE_INFO,
	GTK_MESSAGE_WARNING,
	GTK_MESSAGE_ERROR,
	GTK_MESSAGE_QUESTION
};

static GtkButtonsType dlgtypemap[] = {
	GTK_BUTTONS_OK,
	GTK_BUTTONS_OK_CANCEL,
	GTK_BUTTONS_YES_NO
};

static GtkWindow *mainwindow = NULL;

static void ndialog_clear_events()
{
	while (gtk_events_pending())
		gtk_main_iteration();
}

int ndialog_init()
{
	int argc = 0;
	const char *argv[] = {};
	gtk_init(&argc, (char***)&argv);
	mainwindow = (GtkWindow*) gtk_window_new(GTK_WINDOW_TOPLEVEL);
	if (!mainwindow) {
		_ndutil_set_error(NDIALOG_ERR_SYSERR);
		return 0;
	}
	ndialog_clear_events();
	return 1;
}

void ndialog_exit()
{
	if (mainwindow) {
		gtk_widget_destroy((GtkWidget*) mainwindow);
		mainwindow = NULL;
	}
}

ndialog_btntype ndialog_messagebox(const char* const title, const char* const message, const ndialog_msgtype msgtype, const ndialog_dlgtype dlgtype)
{
	_ndutil_clear_error();

	if (!gtk_init_check(NULL, NULL)) {
		_ndutil_set_error(NDIALOG_ERR_SYSERR);
        return -1;
    }

	GtkWidget *dialog = gtk_message_dialog_new(mainwindow, GTK_DIALOG_MODAL, msgtypemap[msgtype], dlgtypemap[dlgtype], "%s", message ? message : "");
	if (title) {
		gtk_window_set_title(GTK_WINDOW(dialog), title);
	}
	gint ok = gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
	ndialog_clear_events();

	switch (ok) {
		case GTK_RESPONSE_OK: return NDIALOG_BTN_OK;
		case GTK_RESPONSE_CANCEL: return NDIALOG_BTN_CANCEL;
		case GTK_RESPONSE_YES: return NDIALOG_BTN_YES;
		case GTK_RESPONSE_NO: return NDIALOG_BTN_NO;
		default:;
	}
	return NDIALOG_BTN_NONE;
}

static int ndialog_addfilters(GtkFileChooser *chooser, const char **filters)
{
	if (!filters || !*filters) return 0;
	int cnt = 0, i = 0, k = 0;

	int fblen = 0;
	const char **f = filters;
	while (*f) {
		fblen += strlen(*f) + 1;
		++f;
	}
	char *buf = malloc(fblen);
	char *pat = malloc(fblen);

	while (filters[i] && filters[i+1]) {
		GtkFileFilter *thefilter = gtk_file_filter_new();
		gtk_file_filter_set_name(thefilter, filters[i]);
		int nextns = _ndutil_separate_extns(filters[i+1], buf, fblen);
		char *p = buf;
		for (k = 0; k < nextns; ++k) {
			sprintf(pat, "*.%s", p);
			gtk_file_filter_add_pattern(thefilter, pat);
			p += strlen(p) + 1;
		}
		gtk_file_chooser_add_filter(chooser, thefilter);
		++cnt;
		i += 2;
	}

	free(pat);
	free(buf);
	return cnt;
}

const char *ndialog_savefile(const char* const title, const char* const path, const char **filters, const int allowcreatefolder, const int askoverwrite)
{
	_ndutil_clear_error();

	if (!gtk_init_check(NULL, NULL)) {
		_ndutil_set_error(NDIALOG_ERR_SYSERR);
        return NULL;
    }

	static char* buf = NULL;
	static int buflen = 0;
	const char *res = buf;

	GtkWidget *dialog = gtk_file_chooser_dialog_new(title, mainwindow, GTK_FILE_CHOOSER_ACTION_SAVE,
		"_Cancel", GTK_RESPONSE_CANCEL, "_Save", GTK_RESPONSE_ACCEPT, NULL);

	GtkFileChooser *chooser = GTK_FILE_CHOOSER(dialog);
	if (askoverwrite) {
		gtk_file_chooser_set_do_overwrite_confirmation(chooser, TRUE);
	}
	gtk_file_chooser_set_create_folders(chooser, allowcreatefolder);
	ndialog_addfilters(chooser, filters);

	if (path && _ndutil_is_file(path)) {
		gtk_file_chooser_set_filename(chooser, path);
	} else if (path && _ndutil_is_dir(path)) {
		gtk_file_chooser_set_current_folder(chooser, path);
		gtk_file_chooser_set_current_name(chooser, "Untitled");
	} else {
		gtk_file_chooser_set_current_name(chooser, "Untitled");
	}

	gint dres = gtk_dialog_run(GTK_DIALOG(dialog));
	if (dres == GTK_RESPONSE_ACCEPT) {
		char *filename = gtk_file_chooser_get_filename(chooser);
		if (filename) {
			if (strlen(filename) + 1 > buflen) {
				buflen = strlen(filename) + 1;
				buf = realloc(buf, buflen);
				res = buf;
			}
			if (buf) {
				strncpy(buf, filename, buflen);
			} else {
				_ndutil_set_error(NDIALOG_ERR_NOMEM);
			}
			g_free(filename);
		} else {
			/* we should not get here! */
			_ndutil_set_error(NDIALOG_ERR_UNKNOWN);
			res = NULL;
		}
	} else {
		res = NULL;
	}

	gtk_widget_destroy(dialog);
	ndialog_clear_events();

	return res;
}

const char *ndialog_openfile(const char* const title, const char* const path, const char **filters, const int allowmultiple, int *nres)
{
	_ndutil_clear_error();

	if (nres) {
		*nres = 0;
	} else if (allowmultiple) {
		_ndutil_set_error(NDIALOG_ERR_INVARG);
		return NULL;
	}

	if (!gtk_init_check(NULL, NULL)) {
		_ndutil_set_error(NDIALOG_ERR_SYSERR);
        return NULL;
    }

	static char* buf = NULL;
	static int buflen = 0;
	const char *res = buf;

	GtkWidget *dialog = gtk_file_chooser_dialog_new(title, mainwindow, GTK_FILE_CHOOSER_ACTION_OPEN,
		"_Cancel", GTK_RESPONSE_CANCEL, "_Open", GTK_RESPONSE_ACCEPT, NULL);

	GtkFileChooser *chooser = GTK_FILE_CHOOSER(dialog);
	gtk_file_chooser_set_select_multiple (chooser, allowmultiple);
	ndialog_addfilters(chooser, filters);

	if (path && _ndutil_is_file(path)) {
		gtk_file_chooser_set_filename(chooser, path);
	} else if (path && _ndutil_is_dir(path)) {
		gtk_file_chooser_set_current_folder(chooser, path);
	}

	gint dres = gtk_dialog_run(GTK_DIALOG(dialog));
	if (dres == GTK_RESPONSE_ACCEPT) {
		if (!allowmultiple) {
			char *filename = gtk_file_chooser_get_filename(chooser);
			if (filename) {
				if (nres) { *nres = 1; }
				if (strlen(filename) + 1 > buflen) {
					buflen = strlen(filename) + 1;
					buf = realloc(buf, buflen);
					res = buf;
				}
				if (buf) {
					strncpy(buf, filename, buflen);
				} else {
					_ndutil_set_error(NDIALOG_ERR_NOMEM);
				}

				g_free(filename);
			} else {
				/* we should not get here! */
				_ndutil_set_error(NDIALOG_ERR_UNKNOWN);
				res = NULL;
			}
		} else {
			GSList *filenames = gtk_file_chooser_get_filenames(chooser);
			if (filenames) {
				int flen = 0;
				GSList *cur = filenames;
				while (cur) {
					flen += strlen((char*)cur->data) + 1;
					cur = cur->next;
				}
				if (flen > buflen) {
					buflen = flen;
					buf = realloc(buf, buflen);
					res = buf;
				}
				char *p = buf;
				cur = filenames;
				while (cur) {
					char* filename = (char*) cur->data;
					strcpy(p, filename);
					p += strlen(filename) + 1;
					g_free(filename);
					cur = cur->next;
					++*nres;
				}
				g_slist_free(filenames);
			} else {
				/* we should not get here! */
				_ndutil_set_error(NDIALOG_ERR_UNKNOWN);
				res = NULL;
			}
		}
	} else {
		res = NULL;
	}

	gtk_widget_destroy(dialog);
	ndialog_clear_events();

	return res;
}

const char * ndialog_selectfolder(const char* const title, const char* const path, const int allowcreatefolder)
{
	_ndutil_clear_error();

	if (!gtk_init_check(NULL, NULL)) {
		_ndutil_set_error(NDIALOG_ERR_SYSERR);
        return NULL;
    }

	static char* buf = NULL;
	static int buflen = 0;
	const char *res = buf;

	GtkWidget *dialog = gtk_file_chooser_dialog_new(title, mainwindow,
		allowcreatefolder ? GTK_FILE_CHOOSER_ACTION_CREATE_FOLDER : GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
		"_Cancel", GTK_RESPONSE_CANCEL, "_Open", GTK_RESPONSE_ACCEPT, NULL);

	GtkFileChooser *chooser = GTK_FILE_CHOOSER(dialog);

	if (path && _ndutil_is_file(path)) {
		gtk_file_chooser_set_filename(chooser, path);
	} else if (path && _ndutil_is_dir(path)) {
		gtk_file_chooser_set_current_folder(chooser, path);
	}

	gint dres = gtk_dialog_run(GTK_DIALOG(dialog));
	if (dres == GTK_RESPONSE_ACCEPT) {
		char *filename = gtk_file_chooser_get_filename(chooser);
		if (filename) {
			if (strlen(filename) + 1 > buflen) {
				buflen = strlen(filename) + 1;
				buf = realloc(buf, buflen);
				res = buf;
			}
			if (buf) {
				strncpy(buf, filename, buflen);
			} else {
				_ndutil_set_error(NDIALOG_ERR_NOMEM);
			}
			g_free(filename);
		} else {
			/* we should not get here! */
			_ndutil_set_error(NDIALOG_ERR_UNKNOWN);
			res = NULL;
		}
	} else {
		res = NULL;
	}

	gtk_widget_destroy(dialog);
	ndialog_clear_events();

	return res;
}
