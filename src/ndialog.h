/* ndialog.h
 *
 * simple interface to native dialogs
 *
 * Gunnar Zötl <gz@tset.de>, 2015
 * Released under the terms of the MIT license. See file LICENSE for details.
 */

#ifndef ndialog_h
#define ndialog_h

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    NDIALOG_DLG_OK,
    NDIALOG_DLG_OKCANCEL,
    NDIALOG_DLG_YESNO
} ndialog_dlgtype;

typedef enum {
    NDIALOG_BTN_NONE,
    NDIALOG_BTN_OK,
    NDIALOG_BTN_CANCEL,
    NDIALOG_BTN_YES,
    NDIALOG_BTN_NO
} ndialog_btntype;

typedef enum {
    NDIALOG_MSG_INFO,
    NDIALOG_MSG_WARNING,
    NDIALOG_MSG_ERROR,
    NDIALOG_MSG_QUESTION
} ndialog_msgtype;

typedef enum {
    NDIALOG_ERR_NONE = 0,   /* no error */
    NDIALOG_ERR_BUF2SMALL,  /* some buffer was too small */
    NDIALOG_ERR_NOMEM,      /* out of memory */
    NDIALOG_ERR_INVENC,     /* invalid source encoding, may happen on non-utf platforms */
    NDIALOG_ERR_INVARG,     /* invalid argument */
    NDIALOG_ERR_SYSERR,     /* internal error in the underlying system */

    NDIALOG_ERR_NOTIMP,     /* this functionality has not been implemented. You should not see this outside of development builds... */
    NDIALOG_ERR_UNKNOWN     /* no idea what went wrong, this one must be last */
} ndialog_errtype;


/* Function: ndialog_init()
 *
 * performs some internal setup of the ndialog library, if needed.
 *
 * Arguments:
 *  -
 *
 * Returns:
 *  0 if there was an error (and you should not use any other ndialog
 *  functions), 1 otherwise.
 */
int ndialog_init();

/* Function: ndialog_exit()
 *
 * perform internal cleanup after use of the ndialog library, if needed.
 *
 * Arguments:
 *  -
 *
 * Returns:
 *  -
 */
void ndialog_exit();

/* Function: ndialog_messagebox
 *
 * displays a messagebox.
 *
 * Arguments:
 *  title   - the window title for the messagebox. May be NULL, in which
 *            case some system default title is used. If not null, this
 *            is expected to be an UTF-8 encoded string.
 *  message - the message to display. May be NULL, in which case "" is
 *            used for the message. If not null, this is expected to be
 *            an UTF-8 encoded string.
 *  msgtype - one of the ndialog_msgtype values. What it does depends on
 *            the system, but probably it changes the icon that goes with
 *            the message box.
 * dlgtype  - one of the ndialog_dlgtype values. Selects which buttons to
 *            display for the user to interact with the messagebox.
 *
 * Returns:
 *  one of the ndialog_btntype values, depending on which button was clicked
 *  by the user. Returns NDIALOG_BTN_NONE if the message box was closed with
 *  escape or by closing its window.
 */
ndialog_btntype ndialog_messagebox(const char* const title, const char* const message, const ndialog_msgtype msgtype, const ndialog_dlgtype dlgtype);

/* Function: ndialog_savefile
 *
 * displays a save file dialog.
 *
 * Arguments:
 *  title   - the window title for the save file dialog. May be NULL, in
 *            which case some system default title is used. If not null,
 *            this is expected to be an UTF-8 encoded string.
 *  path    - the initial path. May be NULL or "", in which case some
 *            system default is used.  If not null, this is expected to
 *            be an UTF-8 encoded string.
 *  filters - an array of strings, with entries on even indices being
 *            the names of the individual filters, and the following
 *            entries a comma (,) separated list of file extensions to
 *            use. The list ends with a NULL. Both extensions and names
 *            are expected to be utf-8 encoded.
 *  allowcreatefolder - a flag, set to 1 if you want the save dialog to
 *            offer the option for the user to create additional folders,
 *            0 if not.
 *  askoverwrite - a flag, set to 1 if the save dialog should confirm
 *              overwriting existing files, 0 if not.
 *
 * Returns:
 *  the full path and name of the selected file. This is an internal static
 *  buffer, so you can't deallocate it, and can't rely on it to keep it's
 *  value across calls to ndialog_savefile. Returns NULL if the dialog was
 *  canceled, or an error occurred.
 */
const char * ndialog_savefile(const char * const title, const char * const path, const char **filters, const int allowcreatefolder, const int askoverwrite);

/* Function: ndialog_openfile
 *
 * displays an open file dialog.
 *
 * Arguments:
 *  title   - the window title for the open file dialog. May be NULL, in
 *            which case some system default title is used. If not null,
 *            this is expected to be an UTF-8 encoded string.
 *  path    - the initial path. May be NULL or "", in which case some
 *            system default is used.  If not null, this is expected to
 *            be an UTF-8 encoded string.
 *  filters - an array of strings, with entries on even indices being
 *            the names of the individual filters, and the following
 *            entries a comma (,) separated list of file extensions to
 *            use. The list ends with a NULL. Both extensions and names
 *            are expected to be utf-8 encoded.
 *  allowmultiple - a flag, set to 1 if you want to allow for the user
 *            to select multiple files, 0 if not.
 *  nres    - a pointer to an integer that receives the number of filenames
 *            returned by this function. If allowmultiple is 0, then this
 *            may be NULL, otherwise not.
 *
 * Returns:
 *  the full path and name of the selected file(s), plus the number of
 *  selected files in *nres, if nres is not NULL. If multiple files are
 *  returned, they are returned in one string, separated by zero chars
 *  ('\0'), and the number of results returned in *nres. This is an
 *  internal static buffer, so you can't deallocate it, and can't rely on
 *  it to keep it's value across calls to ndialog_openfile. Returns NULL
 *  if the dialog was canceled, or an error occurred.
 */
const char * ndialog_openfile(const char * const title, const char * const path, const char **filters, const int allowmultiple, int* nres);

/* Function: ndialog_selectfolder
 *
 * displays a select folder dialog.
 *
 * Arguments:
 *  title   - the window title for the select folder dialog. May be NULL,
 *            in which case some system default title is used. If not null,
 *            this is expected to be an UTF-8 encoded string.
 *  path    - the initial path. May be NULL or "", in which case some
 *            system default is used.  If not null, this is expected to
 *            be an UTF-8 encoded string.
 *  allowcreatefolder - a flag, set to 1 if you want the save dialog to
 *            offer the option for the user to create additional folders,
 *            0 if not.
 *
 * Returns:
 *  the full path and name of the selected folder. This is an internal
 *  static buffer, so you can't deallocate it, and can't rely on it to
 *  keep it's value across calls to ndialog_selectfolder. Returns NULL
 *  if the dialog was canceled, or an error occurred.
 */
const char * ndialog_selectfolder(const char * const title, const char * const path, const int allowcreatefolder);

/* Function: ndialog_lasterror
 *
 * retrieve the last error that occurred inside of ndialog. Also resets
 * the last error to NDIALOG_ERR_NONE. Note that any call to ndialog resets
 * the last error.
 *
 * Arguments:
 *  -
 *
 * Returns:
 *  the id of the last error that occurred (see ndialog_errtype above)
 */
ndialog_errtype ndialog_lasterror();

#ifdef __cplusplus
}
#endif

#endif /* ndialog_h */
