/* ndialog_macosx.m
 *
 * simple interface to native dialogs, MacOS X implementation
 *
 * Gunnar Zötl <gz@tset.de>, 2015
 * Released under the terms of the MIT license. See file LICENSE for details.
 */

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#include <stdlib.h>
#include <string.h>

#include "ndialog.h"
#include "ndialog_util.h"

int ndialog_init()
{
	return 1;
}

void ndialog_exit()
{
	// empty
}

ndialog_btntype ndialog_messagebox(const char* const title, const char* const message, const ndialog_msgtype msgtype, const ndialog_dlgtype dlgtype)
{
	_ndutil_clear_error();
	ndialog_btntype res = NDIALOG_BTN_NONE;

	NSAlert *alert = [[NSAlert alloc] init];
	if (dlgtype == NDIALOG_DLG_OK) {
		[alert addButtonWithTitle:@"OK"];
	} else if (dlgtype == NDIALOG_DLG_OKCANCEL) {
		[alert addButtonWithTitle:@"OK"];
		[alert addButtonWithTitle:@"Cancel"];
	} else if (dlgtype == NDIALOG_DLG_YESNO) {
		[alert addButtonWithTitle:@"Yes"];
		[alert addButtonWithTitle:@"No"];
	} else {
		_ndutil_set_error(NDIALOG_ERR_INVARG);
		return NDIALOG_BTN_NONE;
	}
	NSString *msg = [NSString stringWithUTF8String:(message ? message : "")];
	[alert setMessageText:msg];
	[msg release];

    if (msgtype == NDIALOG_MSG_INFO) {
    	[alert setAlertStyle:NSInformationalAlertStyle];
    } else if (msgtype == NDIALOG_MSG_WARNING) {
    	[alert setAlertStyle:NSWarningAlertStyle];
    } else if (msgtype == NDIALOG_MSG_ERROR) {
    	[alert setAlertStyle:NSCriticalAlertStyle];
    } else {
    	[alert setAlertStyle:NSInformationalAlertStyle];
    }
	
	NSString *ttl = [NSString stringWithUTF8String:(title ? title : "")];
	NSWindow *win = [alert window];
	[win setTitle:ttl];
	[ttl release];

	int btn = [alert runModal];
	if (dlgtype == NDIALOG_DLG_OK) {
		if (btn == NSAlertFirstButtonReturn) res = NDIALOG_BTN_OK;
	} else if (dlgtype == NDIALOG_DLG_OKCANCEL) {
		if (btn == NSAlertFirstButtonReturn)
			res = NDIALOG_BTN_OK;
		else if (btn == NSAlertSecondButtonReturn)
			res = NDIALOG_BTN_CANCEL;
	} else if (dlgtype == NDIALOG_DLG_YESNO) {
		if (btn == NSAlertFirstButtonReturn)
			res = NDIALOG_BTN_YES;
		else if (btn == NSAlertSecondButtonReturn)
			res = NDIALOG_BTN_NO;
	}

	[alert release];

	return res;
}

static NSArray* ndialog_makefilters(const char **filters, int *all_others)
{
	*all_others = 0;
	if (!filters || !*filters) return 0;
	NSMutableArray *res = [NSMutableArray array];

	while (*filters) {
		filters += 1;
		if (**filters == '*') {
			*all_others = 1;
			filters += 1;
			continue;
		}
		char *the_filter = strdup(*filters), *filter = the_filter, *pos;
		NSString *ext = 0;
		while ((pos = strchr(filter, ',')) != 0) {
			*pos = 0;
			ext = [NSString stringWithUTF8String:filter];
			[res addObject: ext];
			*pos = ',';
			filter = pos + 1;
		}
		ext = [NSString stringWithUTF8String:filter];
		[res addObject: ext];
		filters += 1;
		free(the_filter);
	}
	return res;
}

/* TODO: implement askoverwrite */
const char *ndialog_savefile(const char* const title, const char* const path, const char **filters, const int allowcreatefolder, const int askoverwrite)
{
	static char *buf = 0;
	static int buflen = 0;
	const char *res = 0;

	_ndutil_clear_error();

	NSSavePanel *spanel = [NSSavePanel savePanel];

	[spanel setCanCreateDirectories:(allowcreatefolder ? YES : NO)];

	if (title) {
		[spanel setTitle:[NSString stringWithUTF8String:title] ];
	}
	if (filters) {
		int all_others;
		NSArray *filt = ndialog_makefilters(filters, &all_others);
		[spanel setAllowedFileTypes:filt];
		if (all_others) {
			[spanel setAllowsOtherFileTypes:YES];
		}
	}
	if (path) {
		if (!_ndutil_is_dir(path)) {
			char *pbuf = strdup(path);
			int pblen = strlen(pbuf) + 1;
			_ndutil_dirname(path, pbuf, pblen);
			[spanel setDirectoryURL:[NSURL URLWithString:[NSString stringWithUTF8String:pbuf ] ] ];
			_ndutil_filename(path, pbuf, pblen);
			[spanel setNameFieldStringValue:[NSString stringWithUTF8String:pbuf ] ];
			free(pbuf);
		} else {
			[spanel setDirectoryURL:[NSURL URLWithString:[NSString stringWithUTF8String:path ] ] ];
		}
	}

	if ([spanel runModal] == NSFileHandlingPanelOKButton) {
		const char* u8path = spanel.URL.path.UTF8String;
		int u8len = strlen(u8path) + 1;
		if (u8len > buflen) {
			buf = realloc(buf, u8len);
			buflen = u8len;
		}
		strncpy(buf, u8path, u8len);
		res = buf;
	}

	[spanel release];
	return res;
}

const char *ndialog_openfile(const char* const title, const char* const path, const char **filters, const int allowmultiple, int *nres)
{
	static char *buf = 0;
	static int buflen = 0;
	const char *res = 0;

	if (nres) {
		*nres = 0;
	} else if (allowmultiple) {
		_ndutil_set_error(NDIALOG_ERR_INVARG);
		return NULL;
	}

	_ndutil_clear_error();
	NSOpenPanel *opanel = [NSOpenPanel openPanel];
	[opanel setCanChooseFiles:YES];
	[opanel setCanChooseDirectories:NO];

	if (title) {
		[opanel setTitle:[NSString stringWithUTF8String:title] ];
	}
	if (filters) {
		int all_others;
		NSArray *filt = ndialog_makefilters(filters, &all_others);
		[opanel setAllowedFileTypes:filt];
		if (all_others) {
			[opanel setAllowsOtherFileTypes:YES];
		}
	}
	if (path) {
		[opanel setDirectoryURL:[NSURL URLWithString:[NSString stringWithUTF8String:path ] ] ];
	}
	if (allowmultiple) {
		[opanel setAllowsMultipleSelection:YES];
	}

	if ([opanel runModal] == NSFileHandlingPanelOKButton) {
		int len = 0;
		int i;
		for (i = 0; i < opanel.URLs.count; ++i) {
			NSURL *url = [opanel.URLs objectAtIndex:i];
			len += strlen(url.path.UTF8String) + 1;
		}
		if (len > buflen) {
			buf = realloc(buf, len);
			buflen = len;
		}
		char *ptr = buf;
		int l = len;
		for (i = 0; i < opanel.URLs.count; ++i) {
			NSURL *url = [opanel.URLs objectAtIndex:i];
			strncpy(ptr, url.path.UTF8String, l);
			l -= strlen(ptr) + 1;
			ptr += strlen(ptr) + 1;
		}
		if (nres) *nres = opanel.URLs.count;
		res = buf;
	}

	[opanel release];
	return res;
}

const char * ndialog_selectfolder(const char* const title, const char* const path, const int allowcreatefolder)
{
	static char *buf = 0;
	static int buflen = 0;
	const char *res = 0;

	_ndutil_clear_error();
	NSOpenPanel *opanel = [NSOpenPanel openPanel];
	[opanel setCanChooseFiles:NO];
	[opanel setCanChooseDirectories:YES];
	[opanel setAllowsMultipleSelection:NO];
	[opanel setCanCreateDirectories:(allowcreatefolder ? YES : NO)];

	if (title) {
		[opanel setTitle:[NSString stringWithUTF8String:title] ];
	}
	if (path) {
		[opanel setDirectoryURL:[NSURL URLWithString:[NSString stringWithUTF8String:path ] ] ];
	}

	if ([opanel runModal] == NSFileHandlingPanelOKButton) {
		const char* u8path = opanel.URL.path.UTF8String;
		int u8len = strlen(u8path) + 1;
		if (u8len > buflen) {
			buf = realloc(buf, u8len);
			buflen = u8len;
		}
		strncpy(buf, u8path, u8len);
		res = buf;
	}

	[opanel release];
	return res;
}
